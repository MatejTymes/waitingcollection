package sk.matejtymes.task;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Function;

public class MergeLists<T, I extends Iterable<List<T>>> implements Function<I, List<T>>
{
    private final static MergeLists INSTANCE = new MergeLists();

    private MergeLists()
    {
    }

    @Override
    public List<T> apply(I input)
    {
        List<T> result = new ArrayList<T>();

        for (List<T> items : input) {
            result.addAll(items);
        }

        return result;
    }

    @Override
    public String toString()
    {
        return "mergeLists()";
    }

    @SuppressWarnings("unchecked")
    public static <T, I extends Iterable<List<T>>> MergeLists<T, I> getInstance()
    {
        return (MergeLists<T, I>) INSTANCE;
    }
}
