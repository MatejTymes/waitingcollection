package sk.matejtymes.task;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Function;

public class ChainFunction<F, T> implements Function<F, T>
{
    private final List<Function<?, ?>> chain;

    public ChainFunction(Function<F, T> function)
    {
        this(new ArrayList<Function<?, ?>>(), function);
    }

    public <TX> ChainFunction(ChainFunction<F, TX> chainFunction, Function<TX, T> function)
    {
        this(new ArrayList<Function<?, ?>>(chainFunction.chain), function);
    }

    private ChainFunction(List<Function<?, ?>> chain, Function<?, T> function)
    {
        this.chain = chain;
        if (function instanceof ChainFunction)
        {
            this.chain.addAll(((ChainFunction<?, ?>) function).chain);
        }
        else
        {
            this.chain.add(function);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public T apply(F input)
    {
        Object result = input;
        for (Function function : chain)
        {
            result = function.apply(result);
        }
        return (T) result;
    }

    public static <F, T> ChainFunction<F, T> chainFunction(Function<F, T> function)
    {
        return (function instanceof ChainFunction) ? (ChainFunction<F, T>) function : new ChainFunction<F, T>(function);
    }

    public static <F, T, T2> ChainFunction<F, T2> chainFunction(ChainFunction<F, T> chainFunction, Function<T, T2> secondFunction)
    {
        return new ChainFunction<F, T2>(chainFunction, secondFunction);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        boolean first = true;
        for (Function function : chain)
        {
            if (!first)
            {
                sb.append(" -> ");
            }
            else
            {
                first = false;
            }
            sb.append(function);
        }

        return sb.toString();
    }
}
