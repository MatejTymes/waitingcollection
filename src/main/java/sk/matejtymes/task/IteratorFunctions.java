package sk.matejtymes.task;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Function;

public class IteratorFunctions
{
    public static <F, T> Function<List<F>, List<T>> onListItems(Function<F, T> itemFunction)
    {
        return new ListIteratorFunction<F, T>(itemFunction);
    }

    public static <F, T> Function<Set<F>, Set<T>> onSetItems(Function<F, T> itemFunction)
    {
        return new SetIteratorFunction<F, T>(itemFunction);
    }

    private static class ListIteratorFunction<F, T> implements Function<List<F>, List<T>>
    {
        private final Function<F, T> itemFunction;

        public ListIteratorFunction(Function<F, T> itemFunction)
        {
            this.itemFunction = itemFunction;
        }

        @Override
        public List<T> apply(List<F> input)
        {
            List<T> result = new ArrayList<T>(input.size());
            for (F item : input)
            {
                result.add(itemFunction.apply(item));
            }
            return result;
        }

        @Override
        public String toString()
        {
            return "forEachListItem(" + itemFunction + ")";
        }
    }

    private static class SetIteratorFunction<F, T> implements Function<Set<F>, Set<T>>
    {
        private final Function<F, T> itemFunction;

        public SetIteratorFunction(Function<F, T> itemFunction)
        {
            this.itemFunction = itemFunction;
        }

        @Override
        public Set<T> apply(Set<F> input)
        {
            Set<T> result = new LinkedHashSet<T>(input.size());
            for (F item : input)
            {
                result.add(itemFunction.apply(item));
            }
            return result;
        }

        @Override
        public String toString()
        {
            return "forEachSetItem(" + itemFunction + ")";
        }
    }
}
