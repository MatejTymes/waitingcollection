package sk.matejtymes.task;

import java.util.LinkedHashSet;
import java.util.Set;

import com.google.common.base.Function;

public class MergeSets<T, I extends Iterable<Set<T>>> implements Function<I, Set<T>>
{
    private final static MergeSets INSTANCE = new MergeSets();

    private MergeSets()
    {
    }

    @Override
    public Set<T> apply(I input)
    {
        Set<T> result = new LinkedHashSet<T>();

        for (Set<T> items : input)
        {
            result.addAll(items);
        }

        return result;
    }

    @Override
    public String toString()
    {
        return "mergeSets()";
    }

    @SuppressWarnings("unchecked")
    public static <T, I extends Iterable<Set<T>>> MergeSets<T, I> getInstance()
    {
        return (MergeSets<T, I>) INSTANCE;
    }
}
