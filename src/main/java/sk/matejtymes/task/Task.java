package sk.matejtymes.task;

import java.util.List;
import java.util.Set;

import com.google.common.base.Function;

import static sk.matejtymes.task.ChainFunction.chainFunction;

public class Task<F, T>
{
    private final ChainFunction<F, T> chain;
    private final Runnable onSuccess;
    private final Runnable onFailure;

    public Task(Function<F, T> function, Runnable onSuccess, Runnable onFailure)
    {
        this.chain = chainFunction(function);
        this.onSuccess = onSuccess;
        this.onFailure = onFailure;
    }

    public Task(Function<F, T> function)
    {
        this(function, null, null);
    }

    public <TX> Task(Task<F, TX> task, Function<TX, T> function, Runnable onSuccess, Runnable onFailure)
    {
        this.chain = chainFunction(task.chain, function);
        this.onSuccess = onSuccess;
        this.onFailure = onFailure;
    }

    public <TX> Task(Task<F, TX> task, Function<TX, T> function)
    {
        this(task, function, task.onSuccess, task.onFailure);
    }

    public T runFor(F input)
    {
        boolean success = false;
        try
        {
            T response = chain.apply(input);

            success = true;

            return response;
        }
        finally
        {
            if (success)
            {
                if (onSuccess != null)
                {
                    onSuccess.run();
                }
            }
            else
            {
                if (onFailure != null)
                {
                    onFailure.run();
                }
            }
        }
    }

    public <T2> Task<F, T2> then(Function<T, T2> function)
    {
        return new Task<F, T2>(this, function);
    }

    public <T2, I extends Iterable<Set<T2>>> Task<F, Set<T2>> thenOnSets(Function<T, I> function)
    {
        return then(function).then(MergeSets.<T2, I>getInstance());
    }

    public <T2, I extends Iterable<List<T2>>> Task<F, List<T2>> thenOnLists(Function<T, I> function)
    {
        return then(function).then(MergeLists.<T2, I>getInstance());
    }


    public static <F, T> Task<F, T> first(Function<F, T> function)
    {
        return new Task<F, T>(function);
    }

    public static <F, T, I extends Iterable<Set<T>>> Task<F, Set<T>> taskForSets(Function<F, I> function)
    {
        return new Task<F, I>(function).then(MergeSets.<T, I>getInstance());
    }

    public static <F, T, I extends Iterable<List<T>>> Task<F, List<T>> taskForLists(Function<F, I> function)
    {
        return new Task<F, I>(function).then(MergeLists.<T, I>getInstance());
    }

    public Task<F, T> onSuccess(Runnable onSuccess)
    {
        return new Task<F, T>(this.chain, onSuccess, this.onFailure);
    }

    public Task<F, T> onFailure(Runnable onFailure)
    {
        return new Task<F, T>(this.chain, this.onSuccess, onFailure);
    }

    @Override
    public String toString()
    {
        return "Task{" +
                "chain=" + chain +
                '}';
    }
}
