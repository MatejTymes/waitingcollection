package sk.matejtymes.waiting;

import java.util.Collection;

/**
 * @author mtymes
 * @since 5/13/13 11:06 PM
 */
// TODO: equals and hashCode implementation is missing
public class QuickClearWaitingLinkedCollection<T> implements WaitingCollection<T> {

    private volatile WaitingLinkedCollection data = new WaitingLinkedCollection();

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty();
    }

    @Override
    public boolean contains(Object element) {
        return data.contains(element);
    }

    @Override
    public boolean add(T element) {
        return data.add(element);
    }

    @Override
    public boolean remove(Object element) {
        return data.remove(element);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return data.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        return data.addAll(collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return data.removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return data.retainAll(collection);
    }

    @Override
    public Object[] toArray() {
        return data.toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return (T1[]) data.toArray(a);
    }

    @Override
    public void clear() {
        data = new WaitingLinkedCollection();
    }

    @Override
    public WaitingIterator<T> iterator() {
        return data.iterator();
    }

    @Override
    public String toString() {
        return data.toString();
    }
}
