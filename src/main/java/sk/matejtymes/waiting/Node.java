package sk.matejtymes.waiting;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author mtymes
 * @since 5/13/13 10:59 PM
 */
class Node<T> {

    private final CountDownLatch nextValueLatch = new CountDownLatch(1);
    private final AtomicReference<Node<T>> next = new AtomicReference<Node<T>>();
    private final AtomicBoolean isActive = new AtomicBoolean(true);
    private final T value;

    public Node(T value) {
        this.value = value;
    }

    public boolean hasNextActive() {
        return getNextActive() != null;
    }

    public Node<T> getNextActive() {
        do {
            Node<T> nextNode = getNext();
            if (nextNode == null || nextNode.isActive()) {
                return nextNode;
            } else if (nextNode.isLastNode()) {
                return null;
            } else {
                // replace inactive node with its child
                next.compareAndSet(nextNode, nextNode.getNext());
            }
        } while (true);
    }

    public Node<T> getNextActive(long timeout, TimeUnit timeUnit) {
        long msTimeout = timeUnit.toMillis(timeout);
        long startTime = System.currentTimeMillis();

        do {
            Node<T> nextNode = getNext();
            if (nextNode == null) {
                long spendTime = System.currentTimeMillis() - startTime;
                try {
                    nextValueLatch.await(msTimeout - spendTime, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    // do nothing
                }
                if (nextValueLatch.getCount() > 0) {
                    return null;
                }
            } else if (nextNode.isActive()) {
                return nextNode;
            } else if (nextNode.isLastNode()) {
                return null;
            } else {
                // replace inactive node with its child
                next.compareAndSet(nextNode, nextNode.getNext());
            }
        } while (true);
    }

    public Node<T> add(T value) {
        Node<T> newNode = new Node<T>(value);

        Node<T> lastNode = this;

        while (!lastNode.next.compareAndSet(null, newNode)) {
            lastNode = lastNode.next.get();
        }
        lastNode.nextValueLatch.countDown();

        return newNode;
    }

    public boolean deactivate() {
        return isActive.compareAndSet(true, false);
    }

    public T getValue() {
        return value;
    }

    private boolean isActive() {
        return isActive.get();
    }

    private boolean isLastNode() {
        return next.get() == null;
    }

    private Node<T> getNext() {
        return next.get();
    }

    static <T> Node<T> newInactiveNode() {
        Node<T> node = new Node<T>(null);
        node.deactivate();
        return node;
    }
}
