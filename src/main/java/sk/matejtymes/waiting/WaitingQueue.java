package sk.matejtymes.waiting;

import java.util.Queue;

/**
 * @author mtymes
 * @since 5/18/13 1:24 AM
 */
public interface WaitingQueue<T> extends Queue<T>, WaitingCollection<T> {

    @Override
    WaitingIterator<T> iterator();
}
