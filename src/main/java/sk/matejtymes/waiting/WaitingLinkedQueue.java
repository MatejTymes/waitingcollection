package sk.matejtymes.waiting;

import java.util.AbstractQueue;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author mtymes
 * @since 5/18/13 1:44 AM
 */
// TODO: equals and hashCode implementation is missing
public class WaitingLinkedQueue<T> extends AbstractQueue<T> implements WaitingQueue<T> {

    private final AtomicInteger size = new AtomicInteger(0);
    private volatile Node<T> initial = Node.newInactiveNode();
    private volatile Node<T> last = initial;

    @Override
    public int size() {
        return size.get();
    }

    @Override
    public boolean offer(T element) {
        last = last.add(element);
        size.incrementAndGet();
        return true;
    }

    @Override
    public T poll() {
        WaitingIterator<T> iterator = iterator();

        T value;
        try {
            value = iterator.next();
            iterator.remove();
        } catch (NoSuchElementException e) {
            value = null;
        }

        return value;
    }

    @Override
    public T peek() {
        T value;
        try {
            value = iterator().next();
        } catch (NoSuchElementException e) {
            value = null;
        }

        return value;
    }

    @Override
    public WaitingIterator<T> iterator() {
        return new WaitingNodeIterator<T>(initial, size);
    }

    @Override
    public void clear() {
        super.clear();
        // optional: this removes references to deactivated nodes
        iterator().hasNext();
    }
}
