package sk.matejtymes.waiting;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
* @author mtymes
* @since 5/15/13 9:05 PM
*/
// TODO: equals and hashCode implementation is missing
public class WaitingLinkedCollection<T> extends AbstractCollection<T> implements WaitingCollection<T> {

    private final AtomicInteger size = new AtomicInteger(0);
    private volatile Node<T> initial = Node.newInactiveNode();
    private volatile Node<T> last = initial;

    @Override
    public int size() {
        return size.get();
    }

    @Override
    public boolean add(T element) {
        last = last.add(element);
        size.incrementAndGet();
        return true;
    }

    @Override
    public Object[] toArray() {
        return new ArrayList<Object>(this).toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return new ArrayList<T>(this).toArray(a);
    }

    @Override
    public WaitingIterator<T> iterator() {
        return new WaitingNodeIterator<T>(initial, size);
    }

    @Override
    public void clear() {
        super.clear();
        // optional: this removes references to deactivated nodes
        iterator().hasNext();
    }
}
