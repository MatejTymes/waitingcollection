package sk.matejtymes.waiting;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * @author mtymes
 * @since 5/14/13 8:08 PM
 */
public interface WaitingIterator<T> extends Iterator<T> {

    T next(long delay, TimeUnit timeUnit) throws TimeoutException;

    boolean removeCurrent();
}
