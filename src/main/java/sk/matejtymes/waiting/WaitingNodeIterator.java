package sk.matejtymes.waiting;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author mtymes
 * @since 5/15/13 9:29 PM
 */
public class WaitingNodeIterator<T> implements WaitingIterator<T> {

    private final AtomicReference<Node<T>> previous = new AtomicReference<Node<T>>();
    private final AtomicInteger size;

    public WaitingNodeIterator(Node<T> previous, AtomicInteger size) {
        this.previous.set(previous);
        this.size = size;
    }

    @Override
    public boolean hasNext() {
        return previous.get().hasNextActive();
    }

    @Override
    public T next() {

        Node<T> previousNode;
        Node<T> nextNode;
        do {
            previousNode = previous.get();
            nextNode = previousNode.getNextActive();
            if (nextNode == null) {
                throw new NoSuchElementException();
            }
        } while (!previous.compareAndSet(previousNode, nextNode));

        return nextNode.getValue();
    }

    @Override
    public T next(long timeout, TimeUnit timeUnit) throws TimeoutException {

        Node<T> previousNode;
        Node<T> nextNode;
        do {
            previousNode = previous.get();
            nextNode = previousNode.getNextActive(timeout, timeUnit);
            if (nextNode == null) {
                throw new TimeoutException("No value received in " + timeout + " " + timeUnit.toString());
            }
        } while (!previous.compareAndSet(previousNode, nextNode));

        return nextNode.getValue();
    }

    @Override
    public void remove() {
        // todo: it is possible to call this before calling the next() method - which is pointless - fix it ???
        if (previous.get().deactivate()) {
            size.decrementAndGet();
        }
    }

    @Override
    public boolean removeCurrent() {
        // todo: it is possible to call this before calling the next() method - which is pointless - fix it ???
        boolean removed = previous.get().deactivate();
        if (removed) {
            size.decrementAndGet();
        }
        return removed;
    }
}
