package sk.matejtymes.waiting;

/**
 * @author mtymes
 * @since 5/15/13 11:43 PM
 */
public interface WaitingIterable<T> extends Iterable<T> {

    @Override
    WaitingIterator<T> iterator();
}
