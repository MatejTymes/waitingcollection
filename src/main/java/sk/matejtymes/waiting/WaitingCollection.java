package sk.matejtymes.waiting;

import java.util.Collection;

/**
 * @author mtymes
 * @since 5/15/13 9:23 PM
 */
public interface WaitingCollection<T> extends Collection<T>, WaitingIterable<T> {

    @Override
    WaitingIterator<T> iterator();
}
