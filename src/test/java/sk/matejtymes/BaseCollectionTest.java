package sk.matejtymes;

import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author mtymes
 * @since 5/13/13 11:50 PM
 */
// todo: add testing of all other methods
public abstract class BaseCollectionTest {

    private Collection<String> collection = getCollection();

    @Test
    public void shouldVerifyNewCollectionIsEmpty() {
        // When & Then
        assertThat(collection.isEmpty(), is(true));
        assertThat(collection.size(), is(0));
        assertThat(collection.iterator().hasNext(), is(false));
    }

    @Test
    public void shouldAddAllValues() {
        // When
        collection.add("Hello");
        collection.add("World");
        collection.add("!");

        // Then
        assertThat(collection, containItems("Hello", "World", "!"));
        assertThat(collection.isEmpty(), is(false));
        assertThat(collection.size(), is(3));
    }

    @Test
    public void shouldThrowExceptionWhenRequestingNonExistingItem() {
        // Given
        collection.add("Hello");
        Iterator<String> iterator = collection.iterator();

        // When & Then
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is("Hello"));
        assertThat(iterator.hasNext(), is(false));
        try {
            iterator.next();
            fail("Expected NoSuchElementException");
        } catch (NoSuchElementException e) {
            // expected
        }
    }

    @Test
    public void shouldAddAllCollectionValues() {
        // When
        collection.addAll(asList("Hello", "World", "!"));

        // Then
        assertThat(collection, containItems("Hello", "World", "!"));
        assertThat(collection.isEmpty(), is(false));
        assertThat(collection.size(), is(3));
    }

    @Test
    public void shouldClearAllValues() {
        // Given
        collection.addAll(asList("Hello", "World", "!"));

        // When
        collection.clear();

        // Then
        assertThat(collection.isEmpty(), is(true));
        assertThat(collection.size(), is(0));
        assertThat(collection.iterator().hasNext(), is(false));
    }

    @Test
    public void shouldAddValuesAfterClearing() {
        // Given
        collection.addAll(asList("Hello", "World", "!"));

        // When
        collection.clear();
        collection.addAll(asList("a", "b", "c", "d"));

        // Then
        assertThat(collection, containItems("a", "b", "c", "d"));
        assertThat(collection.isEmpty(), is(false));
        assertThat(collection.size(), is(4));
    }

    @Test
    public void shouldVerifyElementIsPresent() {
        // Given
        collection.addAll(asList("Hello", "World", "!"));

        // When & Then
        assertThat(collection.contains("World"), is(true));
    }

    @Test
    public void shouldVerifyElementNotIsPresent() {
        // Given
        collection.addAll(asList("Hello", "World", "!"));

        // When & Then
        assertThat(collection.contains("Dog"), is(false));
    }

    @Test
    public void shouldRemoveFirstElement() {
        // Given
        collection.addAll(asList("Hello", "World", "!"));

        // When
        boolean removed = collection.remove("Hello");

        // Then
        assertThat(removed, is(true));
        assertThat(collection, containItems("World", "!"));
        assertThat(collection.isEmpty(), is(false));
        assertThat(collection.size(), is(2));
    }

    @Test
    public void shouldRemoveMiddleElement() {
        // Given
        collection.addAll(asList("Hello", "World", "!"));

        // When
        boolean removed = collection.remove("World");

        // Then
        assertThat(removed, is(true));
        assertThat(collection, containItems("Hello", "!"));
        assertThat(collection.isEmpty(), is(false));
        assertThat(collection.size(), is(2));
    }

    @Test
    public void shouldRemoveLastElement() {
        // Given
        collection.addAll(asList("Hello", "World", "!"));

        // When
        boolean removed = collection.remove("!");

        // Then
        assertThat(removed, is(true));
        assertThat(collection, containItems("Hello", "World"));
        assertThat(collection.isEmpty(), is(false));
        assertThat(collection.size(), is(2));
    }

    @Test
    public void shouldDoNothingWhenRemovingRemovedElement() {
        // Given
        collection.addAll(asList("a", "b", "c"));

        // When
        boolean removed = collection.remove("a");
        boolean removedNonExisting = collection.remove("a");

        // Then
        assertThat(removed, is(true));
        assertThat(removedNonExisting, is(false));
        assertThat(collection, not(contains("a")));
        assertThat(collection, containItems("b", "c"));
        assertThat(collection.isEmpty(), is(false));
        assertThat(collection.size(), is(2));
    }

    @Test
    public void shouldDoNothingWhenRemovingNonExistingElement() {
        // Given
        collection.addAll(asList("Hello", "World", "!"));

        // When
        boolean removed = collection.remove("NotExisting");

        // Then
        assertThat(removed, is(false));
        assertThat(collection, containItems("Hello", "World", "!"));
        assertThat(collection.isEmpty(), is(false));
        assertThat(collection.size(), is(3));
    }

    @Test
    public void shouldRemoveAllElements() {
        // Given
        collection.addAll(asList("Hello", "World", "!"));

        // When
        boolean removed1 = collection.remove("World");
        boolean removed2 = collection.remove("Hello");
        boolean removed3 = collection.remove("!");

        // Then
        assertThat(removed1, is(true));
        assertThat(removed2, is(true));
        assertThat(removed3, is(true));
        assertThat(collection, not(contains("Hello")));
        assertThat(collection, not(contains("World")));
        assertThat(collection, not(contains("!")));
        assertThat(collection.isEmpty(), is(true));
        assertThat(collection.size(), is(0));
        assertThat(collection.iterator().hasNext(), is(false));
    }

//    @Test
//    public void shouldAddAndRemoveManyElements() {
//        // Given
//        Collection<Integer> collection = getCollection();
//        int maxValue = 1000;
//
//        // When & Then
//        for (int i = 0; i <= maxValue; i++) {
//            collection.add(i);
//        }
//        for (int i = maxValue; i >= 0; i--) {
//            collection.remove(i);
//        }
//
//        System.out.println(collection);
//    }

    /* ============================ */
    /* ---   abstract methods   --- */
    /* ============================ */


    protected abstract <T> Collection<T> getCollection();

    protected abstract boolean isOrdered();

    /* ========================== */
    /* ---   helper methods   --- */
    /* ========================== */

    private <T> Matcher<Iterable<? extends T>> containItems(T... items) {
        return isOrdered() ? contains(items) : containsInAnyOrder(items);
    }
}
