package sk.matejtymes;

import org.junit.Test;
import sk.matejtymes.waiting.TimeoutException;
import sk.matejtymes.waiting.WaitingCollection;
import sk.matejtymes.waiting.WaitingIterator;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author mtymes
 * @since 5/15/13 10:08 PM
 */
// todo: test WaitingIterator removeCurrent method
public abstract class BaseWaitingCollectionTest extends BaseCollectionTest {

    private static final String EXPECTED_VALUE = "a";

    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);

    @Test
    public void shouldGetItemFromWaitingIteratorIfItIsAlreadyInCollection() {
        // Given
        WaitingCollection<String> collection = getCollection();
        collection.add(EXPECTED_VALUE);

        // When
        long startTime = now();
        String actualValue = collection.iterator().next(100L, TimeUnit.MILLISECONDS);
        long duration = now() - startTime;

        // Then
        assertThat(actualValue, is(EXPECTED_VALUE));
        assertThat(duration, lessThan(100L));
    }

    @Test
    public void shouldGetItemFromWaitingIteratorIfItIsAddedWithinTimeout() {
        // Given
        WaitingCollection<String> collection = getCollection();

        addValueAfterTimeout(collection, EXPECTED_VALUE, 400L, TimeUnit.MILLISECONDS);

        // When
        long startTime = now();
        String actualValue = collection.iterator().next(1L, TimeUnit.SECONDS);
        long duration = now() - startTime;

        // Then
        assertThat(actualValue, is(EXPECTED_VALUE));
        assertThat(duration, greaterThanOrEqualTo(400L));
        assertThat(duration, lessThan(600L));
    }

    @Test
    public void shouldThrowExceptionIfItemIsNotAdded() {
        // Given
        WaitingCollection<String> collection = getCollection();

        // When
        long startTime = now();
        try {
            collection.iterator().next(500L, TimeUnit.MILLISECONDS);
            fail("expected TimeoutException");
        } catch (TimeoutException e) {
            // expected
        }
        long duration = now() - startTime;

        // Then
        assertThat(duration, greaterThanOrEqualTo(500L));
    }

    @Test
    public void shouldThrowExceptionIfItemIsAddedAfterTimeout() {
        // Given
        WaitingCollection<String> collection = getCollection();

        addValueAfterTimeout(collection, EXPECTED_VALUE, 1L, TimeUnit.SECONDS);

        // When
        long startTime = now();
        try {
            collection.iterator().next(500L, TimeUnit.MILLISECONDS);
            fail("expected TimeoutException");
        } catch (TimeoutException e) {
            // expected
        }
        long duration = now() - startTime;

        // Then
        assertThat(duration, greaterThanOrEqualTo(500L));
    }

    @Test
    public void shouldPassComplexScenarioUsingWaitingIterator() {
        // Given
        WaitingCollection<String> collection = getCollection();

        collection.add(EXPECTED_VALUE + 1);
        collection.add(EXPECTED_VALUE + 2);
        addValueAfterTimeout(collection, EXPECTED_VALUE + 3, 500L, TimeUnit.MILLISECONDS);
        addValueAfterTimeout(collection, EXPECTED_VALUE + 4, 1L, TimeUnit.SECONDS);
        addValueAfterTimeout(collection, EXPECTED_VALUE + 5, 5L, TimeUnit.SECONDS);

        WaitingIterator<String> iterator = collection.iterator();

        // When
        long startTime = now();

        String actualValue1 = iterator.next(1L, TimeUnit.SECONDS);
        long duration1 = now() - startTime;

        String actualValue2 = iterator.next(1L, TimeUnit.SECONDS);
        long duration2 = now() - startTime;

        String actualValue3 = iterator.next(1L, TimeUnit.SECONDS);
        long duration3 = now() - startTime;

        String actualValue4 = iterator.next(2L, TimeUnit.SECONDS);
        long duration4 = now() - startTime;

        try {
            iterator.next(2L, TimeUnit.SECONDS);
            fail("expected TimeoutException");
        } catch (TimeoutException e) {
            // expected
        }
        long duration5 = now() - startTime;

        // Then
        assertThat(actualValue1, is(EXPECTED_VALUE + 1));
        assertThat(duration1, lessThan(100L));
        assertThat(actualValue2, is(EXPECTED_VALUE + 2));
        assertThat(duration2, lessThan(100L));
        assertThat(actualValue3, is(EXPECTED_VALUE + 3));
        assertThat(duration3, greaterThanOrEqualTo(500L));
        assertThat(duration3, lessThan(600L));
        assertThat(actualValue4, is(EXPECTED_VALUE + 4));
        assertThat(duration4, greaterThanOrEqualTo(1000L));
        assertThat(duration4, lessThan(1100L));
        assertThat(duration5, greaterThanOrEqualTo(1000L + 2000L));
    }

    /* ============================ */
    /* ---   abstract methods   --- */
    /* ============================ */

    @Override
    protected abstract <T> WaitingCollection<T> getCollection();

    /* ========================== */
    /* ---   helper methods   --- */
    /* ========================== */

    private long now() {
        return System.currentTimeMillis();
    }

    private <T> void addValueAfterTimeout(final WaitingCollection<T> collection, final T value, long delay, TimeUnit timeUnit) {
        scheduledExecutorService.schedule(new Runnable() {
            @Override
            public void run() {
                collection.add(value);
            }
        }, delay, timeUnit);
    }
}
