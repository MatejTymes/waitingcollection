package sk.matejtymes.waiting;

import sk.matejtymes.BaseWaitingCollectionTest;

/**
 * @author mtymes
 * @since 5/15/13 9:18 PM
 */
public class WaitingLinkedCollectionTest extends BaseWaitingCollectionTest {

    @Override
    protected <T> WaitingCollection<T> getCollection() {
        return new WaitingLinkedCollection<T>();
    }

    @Override
    protected boolean isOrdered() {
        return true;
    }
}
