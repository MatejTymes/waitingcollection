package sk.matejtymes.waiting;

import sk.matejtymes.BaseCollectionTest;
import sk.matejtymes.BaseWaitingCollectionTest;

import java.util.Collection;

/**
 * @author mtymes
 * @since 5/15/13 9:18 PM
 */
public class QuickClearWaitingLinkedCollectionTest extends BaseWaitingCollectionTest {

    @Override
    protected <T> WaitingCollection<T> getCollection() {
        return new QuickClearWaitingLinkedCollection<T>();
    }

    @Override
    protected boolean isOrdered() {
        return true;
    }
}
