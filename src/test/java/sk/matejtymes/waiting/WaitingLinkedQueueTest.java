package sk.matejtymes.waiting;

import sk.matejtymes.BaseWaitingCollectionTest;

/**
 * @author mtymes
 * @since 5/15/13 9:18 PM
 */
// todo: add remaining methods tests
public class WaitingLinkedQueueTest extends BaseWaitingCollectionTest {

    @Override
    protected <T> WaitingCollection<T> getCollection() {
        return new WaitingLinkedQueue<T>();
    }

    @Override
    protected boolean isOrdered() {
        return true;
    }
}
